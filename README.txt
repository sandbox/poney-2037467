/**
 * @file
 * README file for Workbench Moderation Multiple.
 */

Workbench Moderation Multiple
Workbench Moderation extension to support multiple workflow.

CONTENTS
--------

1.  Introduction
2.  Installation
3.  Configuration
3.1  Configuring states
3.2  Configuring conditions
3.3  Configuring workflows
3.4  Configuring transitions
3.5  Visualize workflows
3.6  Import and export workflows
3.7  Permissions
4.  Using the module
5.  Developer notes
6.  Feature roadmap

----
1.  Introduction

Workbench Moderation Multiple lets you define multiple workflows on top of
Workbench Moderation, and use conditions based on tokens to define which
workflow should be used for a given piece of content.

Workbench Moderation Multiple does not interfere with Workbench Moderation in
the sense that it uses the very same arbitrary states and workflow engine.
It only extends the concept of transition by letting users attach it to defined
workflows, trigger actions after a transition has been made, etc

----
2.  Installation

Install the module and enable it according to Drupal standards.

After installation, visit the configuration pages at:

    Admin > Configuration > Workbench > Workbench Moderation

You will get four extra tabs, compared to Workbench Moderation alone.

----
3.  Configuration

Workbench Moderation's configuration section is located at:

    Admin > Configuration > Workbench > Workbench Moderation

This administration section provides tabs to configure states, conditions,
transitions, and workflows. There are also tabs to visualize workflows the
way you built them, and to import and export workflows to JSON.

----
3.1  Configuring states

You will find state configuration at :

    Admin > Configuration > Workbench > Workbench Moderation > States
    
State configuration page works absolutely the same as in Workbench Moderation,
except you will get an extra description field for states.

----
3.2  Configuring conditions

You will find condition configuration at :

    Admin > Configuration > Workbench > Workbench Moderation > Conditions
    
Conditions are a combination of a token-powered string that gets evaluated on a 
node, an operator, and a value. For instance :

Token: [node:language]
Operator: Equals to / Matches
Value: en*

That will create a condition on node language, which will evaluate to true for
any node having a language code starting with "en".

Conditions are evaluated every time Workbench Moderation needs to know what 
transitions it is allowed to perform.

----
3.3  Configuring workflows

You will find workflow configuration at :

    Admin > Configuration > Workbench > Workbench Moderation > Workflows
    
Workflows are enabled when a set of conditions is evaluated to true. For
instance, you can create a workflow that will be get activated for non
English sticky content of type "Article", and another one for all "Basic
page" content. Workflows are listed in an order list. When a node matches
several workflows, then the lower workflow in the list (the one with the
biggest weight) will be selected. 

----
3.4  Configuring transitions

You will find transition configuration at :

    Admin > Configuration > Workbench > Workbench Moderation > Transitions
    
It is a complete overhaul of the Transitions tab from Workbench Moderation.
Transitions are still obviously from one state to another, but you can :
- Manage permissions directly from the transitions configuration panel
- Select actions that will get triggered after the transition has been made
- Associate a transition with one or several workflows.

Note that, as opposed to Workbench Moderation, the from state / to state couple
does not have to be unique with Workbench Moderation Multiple, so that different
workflows can handle transitions from a state to another differently. For
instance, you can define "Workflow A" where only top managers will be allowed to
validate content, and "Workflow B" on less important content, where any editor
can validate content, and where an action sending an email to top managers gets
sent when they do so.

----
3.5  Visualize workflows

You can visualize workflows at :

    Admin > Configuration > Workbench > Workbench Moderation > Visualize
    
This is a useful feature for people willing to check at a glance if there is a
glitch in their workflow settings.

----
3.6  Import and export workflows

You can import and export workflows at :

    Admin > Configuration > Workbench > Workbench Moderation > Import / Export
    
Currently, workflows are exported in JSON, and there is no way to export or
import only portions of the workflows.

----
3.7  Permissions

Workbench Moderation Multiple introduces no new permissions, but overrides
transition permissions from Workbench Moderation.

----
4.  Using the module

Workbench Moderation Multiple introduces no new concept for editors, other than
what is already available in Workbench Moderation. Depending on the workflow
selected for a given node, Workbench Moderation Multiple will automatically
select the transitions that are available.

You will also see which workflow is selected for a given node in the moderation
block.

----
5.  Developer notes

Workbench Moderation Multiple does not have a mature API.

----
6.  Feature roadmap

* Integrate with Features and allow to export selected workflows only.
* Overhaul the configuration UI to make it workflow-centric and reduce
the number of page refreshs.
