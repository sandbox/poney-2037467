<?php

/**
 * @file
 * Provides moderation links for Views.
 */

/**
 * Overrides the handler used by Workbench Moderation to display
 * moderation links.
 */
class workbench_moderation_multiple_handler_field_links extends views_handler_field {
  
  /**
   * Overriding the render function from Workbench Moderation.
   */
  public function render($values) {
    if ($values->{$this->aliases['current']}) {
      $node = node_load($values->{$this->aliases['nid']}, $values->{$this->aliases['vid']});
      return theme('links', array('links' => workbench_moderation_multiple_get_moderation_links($node, array('query' => array('destination' => $_GET['q'])))));
    }
    return '';
  }

}
