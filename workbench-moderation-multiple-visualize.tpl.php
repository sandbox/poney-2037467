<?php
/**
 * @file
 * Theme's implementation to display workflow graphs.
 *
 * Available variables:
 * - $workflows: Workflow array.
 */
?>

<?php foreach ($workflows as $workflow): ?>
  <h2><?php print $workflow['name']; ?></h2>
  <div id="workflow-<?php print $workflow['id']; ?>"></div>
<?php endforeach; ?>
<script type="text/javascript">

  window.onload = function() {
    
    <?php foreach ($workflows as $workflow): { ?>
    var width = 600;
    var height = 400;
    g<?php print $workflow['id']; ?> = new Graph();

    <?php foreach ($workflow['nodes'] as $node): { ?>
    g<?php print $workflow['id']; ?>.addNode("<?php print $node; ?>", { label : "<?php print $node; ?>" });
    <?php endforeach; ?>
    
    <?php foreach ($workflow['edges'] as $edge): { ?>
    g<?php print $workflow['id']; ?>.addEdge("<?php print $edge[0]; ?>", "<?php print $edge[1]; ?>", { directed: true, label : "<?php print implode(', ', $edge[2]); ?>" });
    <?php endforeach; ?>

    var layouter<?php print $workflow['id']; ?> = new Graph.Layout.Spring(g<?php print $workflow['id']; ?>);
    var renderer<?php print $workflow['id']; ?> = new Graph.Renderer.Raphael('workflow-<?php print $workflow['id']; ?>', g<?php print $workflow['id']; ?>, width, height);
    layouter<?php print $workflow['id']; ?>.layout();
    renderer<?php print $workflow['id']; ?>.draw();
    <?php endforeach; ?>
    
  }
</script>
