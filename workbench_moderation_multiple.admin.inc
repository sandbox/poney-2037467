<?php
/**
 * @file
 * Contains implementations for configuration panes for the module.
 * Also contains theme functions for the same panes.
 */

/**
 * Administration form to create and delete moderation transitions.
 */
function workbench_moderation_multiple_admin_transitions_form($form, &$form_state) {
  $form['transitions'] = array(
    '#tree' => TRUE,
  );

  // List existing transitions.
  $transitions = workbench_moderation_multiple_transitions();
  // Get workflow labels for #options lists.
  $workflows = workbench_moderation_multiple_workflow_labels();
  // Get action labels for #options lists.
  $actions = workbench_moderation_multiple_action_labels();
  // Get roles for #options lists.
  $roles = user_roles();

  foreach ($transitions as $transition) {
    $form['transitions'][$transition->tid]['transition'] = array(
      '#type' => 'value',
      '#value' => $transition,
    );
    $form['transitions'][$transition->tid]['name'] = array(
      '#type' => 'textfield',
      '#size' => 30,
      '#default_value' => $transition->name,
    );
    $form['transitions'][$transition->tid]['from_name'] = array(
      '#type' => 'select',
      '#options' => workbench_moderation_state_labels(),
      '#default_value' => $transition->from_name,
    );
    $form['transitions'][$transition->tid]['to_name'] = array(
      '#type' => 'select',
      '#options' => workbench_moderation_state_labels(),
      '#default_value' => $transition->to_name,
    );
    $form['transitions'][$transition->tid]['delete'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete'),
      '#title_display' => 'invisible',
      '#default_value' => FALSE,
    );
    $form['transitions'][$transition->tid]['roles'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Can perform this transition'),
      '#title_display' => 'invisible',
      '#options' => $roles,
      '#default_value' => explode(',', $transition->role_ids),
    );
    $form['transitions'][$transition->tid]['workflows'] = array(
      '#type' => 'checkboxes',
      '#title' => '&nbsp;',
      '#title_display' => 'invisible',
      '#options' => $workflows,
      '#default_value' => explode(',', $transition->workflow_ids),
    );
    $form['transitions'][$transition->tid]['actions'] = array(
      '#type' => 'checkboxes',
      '#title' => '&nbsp;',
      '#title_display' => 'invisible',
      '#options' => $actions,
      '#default_value' => explode(',', $transition->action_ids),
    );
  }

  // Provide fields to create a new transition.
  $states = workbench_moderation_state_labels();
  array_unshift($states, t('- Choose state -'));
  $element = array();
  $element['name'] = array(
    '#type' => 'textfield',
    '#title' => t('New transition'),
    '#size' => 30,
  );
  $element['from_name'] = array(
    '#type' => 'select',
    '#options' => $states,
  );
  $element['to_name'] = array(
    '#type' => 'select',
    '#options' => $states,
  );
  $element['roles'] = array(
    '#type' => 'checkboxes',
    '#title_display' => 'invisible',
    '#options' => $roles,
    '#default_value' => array(),
  );
  $element['workflows'] = array(
    '#type' => 'checkboxes',
    '#title_display' => 'invisible',
    '#options' => $workflows,
    '#default_value' => array(),
  );
  $element['actions'] = array(
    '#type' => 'checkboxes',
    '#title_display' => 'invisible',
    '#options' => $actions,
    '#default_value' => array(),
  );
  $form['transitions']['new'] = $element;

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Theming function for the transitions administration form.
 *
 * Transforms the transitions administration form into a table.
 */
function theme_workbench_moderation_multiple_admin_transitions_form($variables) {
  $form = $variables['form'];

  $header = array(
    t('Name'),
    t('From state'),
    t('To state'),
    t('Workflows including this transition'),
    t('Roles allowed to perform the transition'),
    t('Actions that will be triggered'),
    array('data' => t('Delete'), 'class' => array('checkbox')),
  );

  $rows = array();
  foreach (element_children($form['transitions']) as $key) {
    $element = &$form['transitions'][$key];
    $row = array('data' => array());
    $row['data']['name'] = drupal_render($element['name']);
    $row['data']['from'] = drupal_render($element['from_name']);
    $row['data']['to'] = drupal_render($element['to_name']);
    $row['data']['workflows'] = drupal_render($element['workflows']);
    $row['data']['roles'] = drupal_render($element['roles']);
    $row['data']['actions'] = drupal_render($element['actions']);
    $row['data']['delete'] = drupal_render($element['delete']);
    $rows[] = $row;
  }
  // @TODO: change this css call.
  drupal_add_css(drupal_get_path('module', 'workbench_moderation') . '/css/workbench_moderation.css');
  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('class' => array('width-auto')),
  ));
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Form submit handler for transitions.
 */
function workbench_moderation_multiple_admin_transitions_form_submit($form, &$form_state) {
  foreach ($form_state['values']['transitions'] as $key => $info) {
    // If the transition is set to be deleted.
    if (isset($info['delete']) && $info['delete'] == 1) {
      workbench_moderation_multiple_transition_delete($info['transition']);
      drupal_set_message(t('Transition from %from_name to %to_name has been deleted.', array('%from_name' => $info['from_name'], '%to_name' => $info['to_name'])));
    }
    // If the transition exists already and may have been updated.
    elseif (isset($info['transition'])) {
      $transition = (object) array(
        'tid' => $info['transition']->tid,
        'name' => $info['name'],
        'from_name' => $info['transition']->from_name,
        'to_name' => $info['transition']->to_name,
        'roles' => array_diff(array_values($info['roles']), array(0)),
        'workflows' => array_diff(array_values($info['workflows']), array(0)),
        'actions' => array_diff(array_values($info['actions']), array(0)),
      );
      workbench_moderation_multiple_transition_save($transition);
    }
    // If a new transition has been submitted.
    elseif ($info['from_name'] != '0' && $info['to_name'] != '0') {
      $transition = (object) array(
        'name' => $info['name'],
        'from_name' => $info['from_name'],
        'to_name' => $info['to_name'],
        'roles' => array_diff(array_values($info['roles']), array(0)),
        'workflows' => array_diff(array_values($info['workflows']), array(0)),
        'actions' => array_diff(array_values($info['actions']), array(0)),
      );
      workbench_moderation_multiple_transition_save($transition);
    }
  }
  drupal_set_message(t('Your settings have been saved.'));
}

/**
 * Administration form for workflows.
 *
 * Administrators can use this form to add, delete, reorder, and
 * update the description for workflows.
 */
function workbench_moderation_multiple_admin_workflows_form($form, &$form_state) {

  $form['workflows'] = array(
    '#tree' => TRUE,
  );

  // List existing states.
  $workflows = workbench_moderation_multiple_workflows();
  foreach ($workflows as $workflow) {
    $form['workflows'][$workflow->wid]['workflow'] = array(
      '#type' => 'value',
      '#value' => $workflow,
    );
    $form['workflows'][$workflow->wid]['title'] = array(
      '#type' => 'textfield',
      '#default_value' => $workflow->title,
      '#maxlength' => 255,
      '#size' => 30,
    );
    $form['workflows'][$workflow->wid]['description'] = array(
      '#type' => 'textfield',
      '#default_value' => $workflow->description,
    );
    $form['workflows'][$workflow->wid]['default_state'] = array(
      '#type' => 'radios',
      '#options' => workbench_moderation_state_labels(),
      '#default_value' => $workflow->default_state,
    );
    $form['workflows'][$workflow->wid]['conditions'] = array(
      '#type' => 'checkboxes',
      '#options' => workbench_moderation_multiple_conditions_labels(),
      '#default_value' => explode(',', $workflow->condition_ids),
    );
    $form['workflows'][$workflow->wid]['weight'] = array(
      '#type' => 'weight',
      '#default_value' => $workflow->weight,
    );
    $form['workflows'][$workflow->wid]['delete'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete'),
      '#title_display' => 'invisible',
      '#default_value' => FALSE,
    );
  }

  // Provide fields to create a new workflow.
  $new_workflow['title'] = array(
    '#title' => t('New workflow'),
    '#type' => 'textfield',
    '#description' => t('Enter a name for the new workflow.'),
    '#maxlength' => 255,
    '#size' => 30,
  );
  $new_workflow['description'] = array(
    '#type' => 'textfield',
    '#maxlength' => 255,
    '#title' => '&nbsp;',
    '#description' => t('Enter a description of the new workflow.')
  );
  $new_workflow['default_state'] = array(
    '#type' => 'radios',
    '#options' => workbench_moderation_state_labels(),
  );
  $new_workflow['conditions'] = array(
    '#type' => 'checkboxes',
    '#options' => workbench_moderation_multiple_conditions_labels(),
  );
  $new_workflow['weight'] = array(
    '#type' => 'weight',
  );
  $form['workflows'][] = $new_workflow;

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Transforms the workflow administration form into a reorderable table.
 */
function theme_workbench_moderation_multiple_admin_workflows_form($variables) {
  $form = $variables['form'];

  drupal_add_tabledrag('workbench-moderation-workflows', 'order', 'sibling', 'workbench-moderation-workflow-weight');

  $header = array(
    t('Name'),
    t('Description'),
    t('Default state'),
    t('Conditions to satisfy to trigger the workflow'),
    array('data' => t('Delete'), 'class' => array('checkbox')),
    t('Weight'),
  );

  $rows = array();
  foreach (element_children($form['workflows']) as $key) {
    $element = &$form['workflows'][$key];

    $row = array(
      'data' => array(),
      'class' => array('draggable'),
    );
    $row['data']['title'] = drupal_render($element['title']);
    $row['data']['description'] = drupal_render($element['description']);
    $row['data']['default_state'] = drupal_render($element['default_state']);
    $row['data']['conditions'] = drupal_render($element['conditions']);
    $row['data']['delete'] = drupal_render($element['delete']);

    $element['weight']['#attributes']['class'] = array('workbench-moderation-workflow-weight');
    $row['data']['weight'] = drupal_render($element['weight']);

    $rows[] = $row;
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'workbench-moderation-workflows')));
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Form submit handler for workflow administration.
 */
function workbench_moderation_multiple_admin_workflows_form_submit($form, &$form_state) {
  foreach ($form_state['values']['workflows'] as $key => $info) {
    // Delete a workflow.
    if (!empty($info['delete'])) {
      workbench_moderation_multiple_workflow_delete($info['workflow']);
      drupal_set_message(t('Moderation workflow %title has been deleted.', array('%title' => $info['title'])));
    }
    // Create or update a workflow (update if WID exists, create if not).
    elseif (!empty($info['title'])) {
      $workflow = (object) array(
        'wid' => isset($info['workflow']) ? $info['workflow']->wid : NULL,
        'title' => $info['title'],
        'description' => $info['description'],
        'default_state' => $info['default_state'],
        'conditions' => array_diff(array_values($info['conditions']), array(0)),
        'weight' => $info['weight'],
      );
      workbench_moderation_multiple_workflow_save($workflow);
    }
  }
  drupal_set_message(t('Your settings have been saved.'));
}

/**
 * Administration form for conditions.
 *
 * Administrators can use this form to add, delete, and
 * update the description for conditions.
 */
function workbench_moderation_multiple_admin_conditions_form($form, &$form_state) {

  $form['conditions'] = array(
    '#tree' => TRUE,
  );

  // List existing states.
  $conditions = workbench_moderation_multiple_conditions();

  foreach ($conditions as $condition) {
    $form['conditions'][$condition->cid]['condition'] = array(
      '#type' => 'value',
      '#value' => $condition,
    );
    $form['conditions'][$condition->cid]['name'] = array(
      '#type' => 'textfield',
      '#default_value' => $condition->name,
      '#maxlength' => 255,
      '#size' => 30,
    );
    $form['conditions'][$condition->cid]['token'] = array(
      '#type' => 'textfield',
      '#size' => 30,
      '#default_value' => $condition->token,
    );
    $form['conditions'][$condition->cid]['op'] = array(
      '#type' => 'select',
      '#options' => array(
        'eq' => t('Equals to / Matches'),
        'gt' => t('Greater than'),
        'lt' => t('Less than'),
        'ne' => t('Not equal to / Does not match'),
      ),
      '#default_value' => $condition->op,
    );
    $form['conditions'][$condition->cid]['value'] = array(
      '#type' => 'textfield',
      '#default_value' => $condition->value,
      '#maxlength' => 255,
      '#size' => 30,
    );
    $form['conditions'][$condition->cid]['delete'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete'),
      '#title_display' => 'invisible',
      '#default_value' => FALSE,
    );
  }

  // Provide fields to create a new condition.
  $new_condition['name'] = array(
    '#title' => t('New condition'),
    '#type' => 'textfield',
    '#description' => t('Enter a name for the new condition.'),
    '#maxlength' => 255,
    '#size' => 30,
  );
  $new_condition['token'] = array(
    '#type' => 'textfield',
    '#size' => 30,
  );
  $new_condition['op'] = array(
    '#type' => 'select',
    '#options' => array(
      '--' => t('- Select an operator -'),
      'eq' => t('Equals to / Matches'),
      'gt' => t('Greater than'),
      'lt' => t('Less than'),
      'ne' => t('Not equal to / Does not match'),
    ),
  );
  $new_condition['value'] = array(
    '#type' => 'textfield',
    '#description' => t('Enter a value to compare the observable to.'),
    '#maxlength' => 255,
    '#size' => 30,
  );
  $form['conditions'][] = $new_condition;

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Transforms the conditions administration form into a themed table.
 */
function theme_workbench_moderation_multiple_admin_conditions_form($variables) {
  $form = $variables['form'];

  $header = array(
    t('Name'),
    t('Token'),
    t('Operator'),
    t('Value'),
    array('data' => t('Delete'), 'class' => array('checkbox')),
  );

  $rows = array();
  foreach (element_children($form['conditions']) as $key) {
    $element = &$form['conditions'][$key];

    $row = array(
      'data' => array(),
    );
    $row['data']['name'] = drupal_render($element['name']);
    $row['data']['token'] = drupal_render($element['token']);
    $row['data']['op'] = drupal_render($element['op']);
    $row['data']['value'] = drupal_render($element['value']);
    $row['data']['delete'] = drupal_render($element['delete']);

    $rows[] = $row;
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= theme('token_tree', array('token_types' => array('node')));
  $output .= drupal_render_children($form);

  return $output;
}

/**
 * Form submit handler for condition administration.
 */
function workbench_moderation_multiple_admin_conditions_form_submit($form, &$form_state) {
  foreach ($form_state['values']['conditions'] as $key => $info) {
    // Delete a condition.
    if (!empty($info['delete'])) {
      workbench_moderation_multiple_condition_delete($info['condition']);
      drupal_set_message(t('Condition %name has been deleted.', array('%name' => $info['name'])));
    }
    // Create or update a condition (depending on whether CID is set or not).
    elseif (!empty($info['name'])) {
      $condition = (object) array(
        'cid' => isset($info['condition']) ? $info['condition']->cid : NULL,
        'name' => $info['name'],
        'token' => $info['token'],
        'op' => $info['op'],
        'value' => $info['value'],
      );
      workbench_moderation_multiple_condition_save($condition);
    }
  }
  drupal_set_message(t('Your settings have been saved.'));
}

/**
 * Simple menu entry that displays graphical representations for the workflows.
 *
 * Graph rendering is done client-side.
 * Will not work on older IE browsers, as it relies on <canvas>.
 */
function workbench_moderation_multiple_visualize() {
  drupal_add_js(drupal_get_path('module', 'workbench_moderation_multiple') . '/js/raphael-min.js');
  drupal_add_js(drupal_get_path('module', 'workbench_moderation_multiple') . '/js/dracula_graffle.js');
  drupal_add_js(drupal_get_path('module', 'workbench_moderation_multiple') . '/js/dracula_graph.js');

  $transitions = workbench_moderation_multiple_transitions();
  $workflows = workbench_moderation_multiple_workflows();
  $states = workbench_moderation_state_labels();
  $roles = user_roles();

  $workflow_graphs = array();
  foreach ($workflows as $workflow) {
    $nodes = array();
    $edges = array();
    foreach ($transitions as $transition) {
      if (in_array($workflow->wid, explode(',', $transition->workflow_ids))) {
        $nodes[] = $transition->from_name;
        $nodes[] = $transition->to_name;
        $roles_raw = explode(',', $transition->role_ids);
        $roles_final = array();
        foreach ($roles_raw as $role) {
          $roles_final[] = $roles[$role];
        }
        $edges[] = array($transition->from_name, $transition->to_name, $roles_final);
      }
    }
    $nodes = array_unique($nodes);
    $workflow_graphs[] = array(
      'id' => $workflow->wid,
      'name' => $workflow->title,
      'nodes' => $nodes,
      'edges' => $edges,
    );
  }

  return theme('workbench_moderation_multiple_visualize', array(
    'workflows' => $workflow_graphs,
  ));
}
