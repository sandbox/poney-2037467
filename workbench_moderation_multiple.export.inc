<?php
/**
 * @file
 * Contains implementations of import and export routines for the multiple
 * workflow configuration keys.
 *
 * In the import and export routines, machine names are generated and consumed
 * on the fly, in order not to rely on scalar IDs. That leads to cleaner, more
 * readable serialized configuration files, and will allow for easier
 * integration with Features.
 *
 * TODO : Make this file a workbench_moderation_multiple.features.inc
 * and add Features integration.
 */

/**
 * Simple form that allows administrators to upload a workflow file.
 */
function workbench_moderation_multiple_import_form($form, &$form_state) {
  $form['json'] = array(
    '#type' => 'file',
    '#title' => t('Workflow file'),
    '#description' => t('Choose a JSON file containing workflow configuration information.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import workflow configuration'),
  );
  return $form;
}

/**
 * Validator that checks that a file has been uploaded and that it is JSON.
 */
function workbench_moderation_multiple_import_form_validate($form, &$form_state) {
  $file = file_save_upload('json', array(
    'file_validate_extensions' => array('json'),
  ));
  if ($file) {
    $form_state['values']['json'] = file_get_contents(drupal_realpath($file->uri));
  }
  else {
    form_set_error('json', t('An error occurred while handling the uploaded file.'));
  }
}

/**
 * Utility function that deletes all entries for the tables passed as arguments.
 *
 * @param array $tables
 *   An array of table names.
 */
function _workbench_moderation_multiple_wipe_db_tables($tables) {
  foreach ($tables as $table) {
    db_delete($table)->execute();
  }
}

/**
 * Workflow import function.
 *
 * Wipes all workflow configuration tables and imports the workflow
 * configuration enclosed in a PHP array.
 *
 * @param array $configuration
 *   A workflow configuration array.
 */
function workbench_moderation_multiple_import($configuration) {
  $serialized_workflow = $configuration;
  $roles = array_flip(user_roles());

  _workbench_moderation_multiple_wipe_db_tables(array(
    'workbench_moderation_multiple_conditions',
    'workbench_moderation_multiple_workflows',
    'workbench_moderation_multiple_transitions',
    'workbench_moderation_states',
    'workbench_moderation_multiple_workflows_conditions',
    'workbench_moderation_multiple_transitions_actions',
    'workbench_moderation_multiple_transitions_workflows',
    'workbench_moderation_multiple_transitions_roles',
  ));

  foreach ($serialized_workflow->states as $state) {
    db_insert('workbench_moderation_states')
      ->fields(array(
        'name' => $state->name,
        'label' => $state->label,
        'description' => $state->description,
        'weight' => $state->weight,
      ))
      ->execute();
  }

  $conditions = array();
  foreach ($serialized_workflow->conditions as $condition) {
    $conditions[$condition->machine_name] = db_insert('workbench_moderation_multiple_conditions')
      ->fields(array(
        'name' => $condition->name,
        'token' => $condition->token,
        'op' => $condition->op,
        'value' => $condition->value,
      ))
      ->execute();
  }

  $workflows = array();
  foreach ($serialized_workflow->workflows as $workflow) {
    $workflows[$workflow->machine_name] = db_insert('workbench_moderation_multiple_workflows')
      ->fields(array(
        'title' => $workflow->title,
        'description' => $workflow->description,
        'default_state' => $workflow->default_state,
        'weight' => $workflow->weight,
      ))
      ->execute();
  }

  $transitions = array();
  foreach ($serialized_workflow->transitions as $transition) {
    $transitions[$transition->machine_name] = db_insert('workbench_moderation_multiple_transitions')
      ->fields(array(
        'from_name' => $transition->from_name,
        'to_name' => $transition->to_name,
      ))
      ->execute();
  }

  foreach ($serialized_workflow->workflows_conditions as $wc) {
    db_insert('workbench_moderation_multiple_workflows_conditions')
      ->fields(array(
        'wid' => $workflows[$wc->wid],
        'cid' => $conditions[$wc->cid],
      ))
      ->execute();
  }

  foreach ($serialized_workflow->transitions_actions as $ta) {
    db_insert('workbench_moderation_multiple_transitions_actions')
      ->fields(array(
        'tid' => $transitions[$ta->tid],
        'aid' => $ta->aid,
      ))
      ->execute();
  }

  foreach ($serialized_workflow->transitions_workflows as $tw) {
    db_insert('workbench_moderation_multiple_transitions_workflows')
      ->fields(array(
        'tid' => $transitions[$tw->tid],
        'wid' => $workflows[$tw->wid],
      ))
      ->execute();
  }

  foreach ($serialized_workflow->transitions_roles as $tr) {
    db_insert('workbench_moderation_multiple_transitions_roles')
      ->fields(array(
        'tid' => $transitions[$tr->tid],
        'rid' => $roles[$tr->rid],
      ))
      ->execute();
  }

  drupal_set_message(t("The workflow configuration has been imported."));
}

/**
 * JSON importer.
 *
 * Wipes all workflow configuration tables and imports the workflow
 * configuration enclosed in a JSON string.
 *
 * @param string $json
 *   A workflow configuration array serialized in JSON.
 */
function workbench_moderation_multiple_import_json($json) {
  workbench_moderation_multiple_import(json_decode($json));
}

/**
 * Submit function for the import form.
 *
 * Calls the import routine on the uploaded JSON file.
 */
function workbench_moderation_multiple_import_form_submit($form, &$form_state) {
  workbench_moderation_multiple_import_json($form_state['values']['json']);
}

/**
 * Turns associative arrays of objects into flat arrays of objects and removes
 * a property from those objects.
 *
 * @param array $array
 *   The array to process.
 * @param object $index
 *   The object property to remove.
 * @return array
 *   The processed, flattened array.
 */
function _workbench_moderation_multiple_array_remove_indexes($array, $index) {
  $clean_array = array();
  foreach (array_values($array) as $item) {
    unset($item->{$index});
    $clean_array[] = $item;
  }
  return $clean_array;
}

/**
 * Gets all the configuration keys from the workflow and returns them.
 *
 * @return array
 *   The full workflow configuration, as a PHP array.
 */
function workbench_moderation_multiple_export() {
  $query = db_select('workbench_moderation_multiple_conditions', 'c')->fields('c', array(
    'cid',
    'token',
    'name',
    'op',
    'value',
  ));
  $query->addExpression('CONCAT_WS(\'::\', c.token, c.op, c.value)', 'machine_name');
  $conditions = $query->execute()->fetchAllAssoc('cid');

  $query = db_select('workbench_moderation_multiple_workflows', 'w')->fields('w', array(
    'wid',
    'title',
    'description',
    'default_state',
    'weight',
  ));
  $workflows = $query->execute()->fetchAllAssoc('wid');
  foreach ($workflows as $key => $workflow) {
    $workflows[$key]->machine_name = preg_replace("/[^a-z0-9]/i", '_', drupal_strtolower($workflow->title));
  }

  $query = db_select('workbench_moderation_multiple_transitions', 't')->fields('t', array(
    'tid',
    'from_name',
    'to_name',
  ));
  $query->addExpression('CONCAT_WS(\'::\', t.from_name, t.to_name)', 'machine_name');
  $transitions = $query->execute()->fetchAllAssoc('tid');

  $query = db_select('workbench_moderation_states', 's')->fields('s', array(
    'name',
    'label',
    'description',
    'weight',
  ));
  $query->addExpression('s.name', 'machine_name');
  $states = $query->execute()->fetchAllAssoc('name');

  $workflows_conditions = db_select('workbench_moderation_multiple_workflows_conditions', 'wc')
    ->fields('wc', array('wid', 'cid'))
    ->execute()
    ->fetchAll();
  foreach ($workflows_conditions as $key => $wc) {
    $workflows_conditions[$key]->wid = $workflows[$wc->wid]->machine_name;
    $workflows_conditions[$key]->cid = $conditions[$wc->cid]->machine_name;
  }

  $transitions_actions = db_select('workbench_moderation_multiple_transitions_actions', 'ta')
    ->fields('ta', array('tid', 'aid'))
    ->execute()
    ->fetchAll();
  foreach ($transitions_actions as $key => $ta) {
    $transitions_actions[$key]->tid = $transitions[$ta->tid]->machine_name;
  }

  $transitions_workflows = db_select('workbench_moderation_multiple_transitions_workflows', 'tw')
    ->fields('tw', array('tid', 'wid'))
    ->execute()
    ->fetchAll();
  foreach ($transitions_workflows as $key => $tw) {
    $transitions_workflows[$key]->wid = $workflows[$tw->wid]->machine_name;
    $transitions_workflows[$key]->tid = $transitions[$tw->tid]->machine_name;
  }

  $roles = user_roles();
  $transitions_roles = db_select('workbench_moderation_multiple_transitions_roles', 'tr')
    ->fields('tr', array('tid', 'rid'))
    ->execute()
    ->fetchAll();
  foreach ($transitions_roles as $key => $tr) {
    $transitions_roles[$key]->tid = $transitions[$tr->tid]->machine_name;
    $transitions_roles[$key]->rid = $roles[$tr->rid];
  }

  $conditions = _workbench_moderation_multiple_array_remove_indexes($conditions, 'cid');
  $workflows = _workbench_moderation_multiple_array_remove_indexes($workflows, 'wid');
  $transitions = _workbench_moderation_multiple_array_remove_indexes($transitions, 'tid');

  $export = array(
    'conditions' => $conditions,
    'workflows' => $workflows,
    'states' => $states,
    'transitions' => $transitions,
    'workflows_conditions' => $workflows_conditions,
    'transitions_actions' => $transitions_actions,
    'transitions_workflows' => $transitions_workflows,
    'transitions_roles' => $transitions_roles,
  );

  return $export;
}

/**
 * Export to file routine.
 *
 * Calls the export routine and sends the generated array to the browser as an
 * attachment converted to JSON.
 */
function workbench_moderation_multiple_export_to_file() {
  header('Content-Type: application/json; name="workflow.json"');
  header('Content-Transfer-Encoding: binary');
  header('Content-Disposition: attachment; filename="workflow.json"');
  header('Expires: 0');
  header('Cache-Control: no-cache, must-revalidate');
  header('Pragma: no-cache');
  print json_encode(workbench_moderation_multiple_export());
}
