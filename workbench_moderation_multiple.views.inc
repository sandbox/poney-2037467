<?php

/**
 * @file
 *  Content moderation views integration tweaking for Workbench.
 */

/**
 * Implements hook_views_data_alter().
 */
function workbench_moderation_multiple_views_data_alter(&$data) {
  $data['workbench_moderation_node_history']['moderation_actions']['field']['handler'] = 'workbench_moderation_multiple_handler_field_links';
}
